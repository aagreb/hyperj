package tk.aarone.hyperj;

import java.util.List;

/**
 * Abstract any kind of HTTP Message either request or response
 */
abstract public class Message {

    /**
     * The Start-Line of the message
     */
    protected StartLine startLine;

    /**
     * Map containing headers of message
     */
    protected List<Header> headers;

    /**
     * Message body of message
     */
    protected String body;

    /**
     * Constructor
     * @param startLine The Start-Line of the Message
     * @param headers The Headers of the Message
     * @param body The body of the message
     */
    public Message(StartLine startLine, List<Header> headers, String body) {
        this.startLine = startLine;
        this.headers = headers;
        this.body = body;
    }

    /**
     * Constructor with empty body
     * @param startLine The Start-Line of the Message
     * @param headers The Headers of the Message
     */
    public Message(StartLine startLine, List<Header> headers) {
        this.startLine = startLine;
        this.headers = headers;
        this.body = "";
    }

    /**
     * Constructor with byte array as body
     * @param startLine The Start-Line of the Message
     * @param headers The Headers of the Message
     * @param body The message body of the message as bytes
     */
    public Message(StartLine startLine, List<Header> headers, byte[] body) {
        this.startLine = startLine;
        this.headers = headers;
        this.body = new String(body);
    }

    /**
     * Generates string representation of HTTP-Message
     * @return String containing http message
     */
    @Override
    public String toString() {
        StringBuilder requestBuilder = new StringBuilder("");
        requestBuilder.append(startLine.toString());
        headers.forEach(header -> requestBuilder.append(header.toString()).append("\r\n"));
        requestBuilder.append("\r\n");
        requestBuilder.append(body);
        return requestBuilder.toString();
    }

}
