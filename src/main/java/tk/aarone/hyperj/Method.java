package tk.aarone.hyperj;

/**
 * This Enum contains all HTTP methods.
 * @see <a href="https://tools.ietf.org/html/rfc2616#section-5.1.1">RFC 2616 - Method</a>
 */
public enum Method {

    OPTIONS,
    GET,
    HEAD,
    POST,
    PUT,
    DELETE,
    TRACE,
    CONNECT;
    /* TODO: This part of the library does not comply with the specification. There should be another HTTP Method that
       is variable. This type of method is called extension method. It is not yet implemented since it's rarely used and
       it is more complicated to implement.
     */

    /**
     * Use name of Enum-Field as default string-representation.
     * @return The Enum-Field name
     */
    @Override
    public String toString() {
        return name();
    }

}
