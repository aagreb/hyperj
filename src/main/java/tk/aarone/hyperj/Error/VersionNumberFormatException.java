package tk.aarone.hyperj.Error;

/**
 * Should be thrown when a HTTP version number is not formatted correctly.
 */
public class VersionNumberFormatException extends RuntimeException {

    /**
     * Override constructor
     * @param message Exception message
     */
    public VersionNumberFormatException(String message) {
        super(message);
    }
}
