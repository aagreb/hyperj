package tk.aarone.hyperj;

import java.util.List;

public class Request extends Message {

    /**
     * Constructor
     * @param requestLine The request-Line of the Request
     * @param headers The headers of the request-Line
     * @param body The body of the request
     */
    public Request(RequestLine requestLine, List<Header> headers, String body) {
        super(requestLine, headers, body);
    }

    /**
     * Constructor
     * @param requestLine The request-Line of the Request
     * @param headers The headers of the request-Line
     */
    public Request(RequestLine requestLine, List<Header> headers) {
        super(requestLine, headers);
    }

    /**
     * Constructor
     * @param requestLine The request-Line of the Request
     * @param headers The headers of the request-Line
     * @param body The body of the request as bytes
     */
    public Request(RequestLine requestLine, List<Header> headers, byte[] body) {
        super(requestLine, headers, body);
    }

}
