package tk.aarone.hyperj;

import java.net.URI;

/**
 * Represents a request line of a request message. This contains method, request-URI and HTTP-Version of request
 * @see <a href="https://tools.ietf.org/html/rfc2616#section-5.1">RFC 2616 - Request-Line</a>
 */
public class RequestLine implements StartLine {

    /**
     * HTTP-Method of Request-Line
     */
    private Method method;

    /**
     * The Request-URI of the Request-Line
     */
    private URI requestUri;

    /**
     * The HTTP-Version of the request line.
     */
    private HttpVersion httpVersion;

    /**
     * Constructor
     * @param method The HTTP method of the Request-Line
     * @param requestUri The Request-URI of the Request-Line
     * @param httpVersion The HTTP-Version of the Request-Line
     */
    public RequestLine(Method method, URI requestUri, HttpVersion httpVersion) {
        this.method = method;
        this.requestUri = requestUri;
        this.httpVersion = httpVersion;
    }

    /**
     * Returns String representation of Request-Line
     * @return String representation according to specification
     */
    @Override
    public String toString() {
        return method + " " + requestUri + " " + httpVersion + "\r\n";
    }

}
