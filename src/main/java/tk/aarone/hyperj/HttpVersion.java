package tk.aarone.hyperj;

import tk.aarone.hyperj.Error.VersionNumberFormatException;

import java.util.regex.Pattern;

/**
 * Represents a HTTP protocol version according to
 * @see <a href="https://tools.ietf.org/html/rfc2616#section-3.1">RFC 2616 - HTTP Version</a>
 */
public enum HttpVersion {

    // At the moment this library only supports HTTP Version 1.1
    ONE_ONE("1", "1");

    /**
     * The major version number
     */
    private String major;

    /**
     * The minor version number
     */
    private String minor;

    /**
     * Constructor for HttpVersion
     * @param major The major version number. MUST only contain numbers
     * @param minor The minor version number. MUST only contain numebrs
     */
    HttpVersion(String major, String minor) {
        setMajor(major);
        setMinor(minor);
    }

    /**
     * Sets and validates major version number
     * @param major The number to set and validate
     */
    public void setMajor(String major) {
        validateVersionNumber(major);
        this.major = removeLeadingZeroes(major);
    }

    /**
     * Sets and validates minor version number
     * @param minor The number to set and validate
     */
    public void setMinor(String minor) {
        validateVersionNumber(minor);
        this.minor = removeLeadingZeroes(minor);
    }

    /**
     * Validates version number
     * @param versionNumber The version number to validate
     */
    public static void validateVersionNumber(String versionNumber) {
        Pattern pattern = Pattern.compile("[0-9]+"); // MUST only contain numbers
        boolean valid = pattern.matcher(versionNumber).matches();

        if (!valid) {
            throw new VersionNumberFormatException("The version number MUST only contain numbers.");
        }
    }

    /**
     * Removes leading zeroes from string
     * @param input The String to remove leading zeroes from
     */
    public static String removeLeadingZeroes(String input) {
        return input.replaceFirst("^0+(?!$)", "");
    }

    /**
     * Returns HTTP version as String
     * @return HTTP version in official representation
     */
    @Override
    public String toString() {
        return "HTTP/" + major + "." + minor;
    }

}
