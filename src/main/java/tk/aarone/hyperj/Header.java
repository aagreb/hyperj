package tk.aarone.hyperj;

/**
 * This class represents a HTTP-Header that can be sent in a request or be retrieved in a response
 */
public class Header {

    /**
     * The name of the header field
     */
    private String name;

    /**
     * The value of the header field
     */
    private String value;

    /**
     * Constructor
     * @param name Name of header field
     * @param value Value of header field
     */
    public Header(String name, String value) {
        this.setName(name);
        this.setValue(value);
    }

    /**
     * Sets and validates name
     * @param name The name to be set and validated
     */
    private void setName(String name) {
        // TODO: Validation
        this.name = name;
    }

    /**
     * Sets and validates value
     * @param value The value to be set and validated
     */
    private void setValue(String value) {
        // TODO: Validation
        this.value = value;
    }

    /**
     * Generates String representation of Header-Field
     * @return header in form of String
     */
    @Override
    public String toString() {
        return name + ":" + value;
    }
}
