package tk.aarone.hyperj;

import org.junit.Test;

import static org.junit.Assert.*;

public class MethodTest {

    @Test
    public void testToString() {
        assertEquals("POST", Method.POST.toString());
    }
}