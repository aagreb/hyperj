package tk.aarone.hyperj;

import org.junit.Test;

import java.net.URI;
import java.net.URISyntaxException;

import static org.junit.Assert.*;

public class RequestLineTest {

    @Test
    public void testToString() throws URISyntaxException {
        String uri = "/img/background.png";
        RequestLine requestLine = new RequestLine(
                Method.GET,
                new URI(uri),
                HttpVersion.ONE_ONE
        );

        assertEquals("GET " + uri + " HTTP/1.1\r\n", requestLine.toString());
    }
}