package tk.aarone.hyperj;

import org.junit.Before;
import org.junit.Test;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;

import static org.junit.Assert.*;

public class RequestTest {

    private Request request;

    @Before
    public void setUp() throws URISyntaxException {
        request = new Request(
                new RequestLine(Method.GET, new URI("/index.html"), HttpVersion.ONE_ONE),
                Arrays.asList(
                        new Header("Content-Length", "123"),
                        new Header("Accept", "text/html")
                ),
                "Testbody"
        );
    }

    @Test
    public void testToString() {
        String expectedRequest = "GET /index.html HTTP/1.1\r\nContent-Length:123\r\nAccept:text/html\r\n\r\nTestbody";
        assertEquals(expectedRequest, request.toString());
    }

}
