package tk.aarone.hyperj;

import org.junit.Test;
import tk.aarone.hyperj.Error.VersionNumberFormatException;

import static org.junit.Assert.*;

public class HttpVersionTest {

    @Test
    public void testValidateVersionNumberNoExceptionOnValid() {
        HttpVersion.validateVersionNumber("001020124");
        HttpVersion.validateVersionNumber("123");
    }

    @Test
    public void testValidateVersionNumberExceptionOnInvalid() {
        try {
            HttpVersion.validateVersionNumber("79asd97");
            fail();
        } catch (VersionNumberFormatException e) {
            // Do nothing
        }

        try {
            HttpVersion.validateVersionNumber("");
            fail();
        } catch (VersionNumberFormatException e) {
            // Do nothing
        }

        try {
            HttpVersion.validateVersionNumber("-124124");
            fail();
        } catch (VersionNumberFormatException e) {
            // Do nothing
        }
    }

    @Test
    public void testRemoveLeadingZeroes() {
        String s = "Hello";
        assertEquals(s, HttpVersion.removeLeadingZeroes(s));

        s = "00Hello";
        assertEquals("Hello", HttpVersion.removeLeadingZeroes(s));

        s = "0000";
        assertEquals("0", HttpVersion.removeLeadingZeroes(s));

        s = "";
        assertEquals("", HttpVersion.removeLeadingZeroes(s));
    }

    @Test
    public void testToString() {
        HttpVersion oneOne = HttpVersion.ONE_ONE;
        assertEquals("HTTP/1.1", oneOne.toString());
    }

}
